<?php 
    require_once('connect.php')
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Shoes</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
          body {
          background-image: url('https://www.gocbao.com/wp-content/uploads/2020/04/anh-mua-thu-dep-7.jpg');
          background-repeat: no-repeat;
          background-attachment: fixed;
          background-size: 100% 100%;
        }

        .table{
            background-color: #ffffff;
        }
    </style>
</head>

<body>
<div class="container" style="color:blue; font-size:18px;"> 
<h1>Search Results</h1>
<?php require_once 'connect.php'; ?> 
<?php
    if (isset($_POST['submit-search']))?>
    <?php
        
        $search = mysqli_real_escape_string($conn, $_POST['search']);
        $sql = "SELECT * FROM shoes WHERE id LIKE '%$search%' OR descriptions LIKE '%$search%' OR size LIKE '%$search%' OR price LIKE '%$search%'";
        $result = mysqli_query($conn, $sql);
        $queryResult = mysqli_num_rows($result);
        

 ?>  
  <div class="row justify-content-center">
            <table class="table">
                <thead>
                    <tr>
                    <th>ID</th>
                        <th>Descriptions</th>
                        <th>Size</th>
                        <th>Price</th>
                        <th colspan="3">Action</th>
                    </tr>
                </thead> 
                   

<?php 

        if ($queryResult > 0 ) {?>
           <?php 
            
            while ($row = mysqli_fetch_assoc($result)){
                echo "<tr><td>" . $row["id"] . "</td><td>" . $row["descriptions"] . "</td><td>" . $row["size"] . "</td><td>" . $row["price"] . "</td>". "<td> 
                <a href=". "Shoesstore.php?edit=".$row['id'] . " ".
               "class="."btn btn-info>Edit</a>" ;
                echo "<a href=". "Shoesstore.php?delete=".$row['id'] . " ".
                "class="."btn btn-info>Delete</a>";
                echo "<a href=". "details.php?details=".$row['id'] . " ".
                "class="."btn btn-info>Detail</a></td>" ;
                echo "</td>";
               echo " </tr>";
            }
            ?>
        <?php} else {
            echo "There are no results matching your search!";

        }?>
<?php    }  ?>
</table>

<a href="shoesstore.php"><button class="btn btn-success" type="back" name="back" >Back</button></a>

</body>
</html>