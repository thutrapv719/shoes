<?php 
    require_once('connect.php')
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Shoes</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
          .class-body {
          background-image: url('https://www.gocbao.com/wp-content/uploads/2020/04/anh-mua-thu-dep-7.jpg');
          background-repeat: no-repeat;
          background-attachment: fixed;
          background-size: 100% 100%;
        }
    </style>
</head>
<body class='class-body'>
<main>
        
    <div>
      <h1 class="text-center" style="color:blue; font-size:38px;">Shoes Store</h1>
      </div>
    <div class="row justify-content-center">
     <form action="connect.php" method="POST" >
      <label style="color:teal; font-size:18px;">ID:</label><br>
      <input type="text" name="id" value="<?php echo $id ?>">
      <br><br>
      <label style="color:teal; font-size:18px;">Descriptions:</label><br>
      <input type="text" name="descriptions" value="<?php echo $descriptions ?>">
      <br><br>
      <label style="color:teal; font-size:18px;">Size:</label><br>
      <input type="text" name="size" value="<?php echo $size ?>">
      <br><br>
      <label style="color:teal; font-size:18px;">Price:</label><br>
      <input type="text" name="price" value="<?php echo $price ?>">
      <br><br>
        <button type="submit" name="create" style="font-size:16px" class="btn btn-success">Create</button>
        <button type="submit" name="save" style="font-size:16px" class="btn btn-success">Save</button>
    </form> 
    </div>
    <!-- Bootstrap table  -->
    <div class="container">
          <form action="search.php" method="POST">   
               <button class="btn btn-success" type="submit" name = "submit-search">Search</button>
               <input style="width:93%"  class= "input-form" type="text" name = "search" >
           </form>
     </div>
    <div class="d-flex table-data">
        <div class="container">
               <table class="table table-striped table-light" style="background-color:tomato">
                <thead class="thead-dark">
                    <tr>
                        <th>ID</th>
                        <th>Descriptions</th>
                        <th>Size</th>
                        <th>Price</th>
                        <th colspan="3">Action</th>
                    </tr>
                    
                    <?php
         
           
                // $result = mysqli_query($GLOBALS['conn'],"SELECT * from shoes LIMIT , 4");
            
                // paginate($result, $sql);
            
           // so luong tren trang , trang hien tai, tong so trang,
           //trang tiep theo

     

        $total_records = $row['total'] ?? 18;
 
        
         $current_page = isset($_GET['page']) ? $_GET['page'] : 1;

        $limit = 3;
 
       
        $total_page = ceil($total_records / $limit);
 
       
         if ($current_page > $total_page){
            $current_page = $total_page;
        }

        else if ($current_page < 1){
            $current_page = 1;
        }

        $start = ($current_page - 1) * $limit;
 
        $result = mysqli_query($GLOBALS['conn'],"SELECT * from shoes LIMIT $start, $limit");

        ?>

        <div>
            <?php 

            while ($row = mysqli_fetch_assoc($result)){
                echo "<tr><td>" . $row["id"] . "</td><td>" . $row["descriptions"] . "</td><td>" . $row["size"] . "</td><td>" . $row["price"] . "</td>". "<td> 
                <a href=". "Shoesstore.php?edit=".$row['id'] . " ".
               "class="."btn btn-info>Edit</a>" ;
                echo "<a href=". "Shoesstore.php?delete=".$row['id'] . " ".
                "class="."btn btn-info>Delete</a>";
                echo "<a href=". "details.php?details=".$row['id'] . " ".
                "class="."btn btn-info>Detail</a></td>" ;
                echo "</td>";
               echo " </tr>";
            }
            ?>
        </div>
        <div class="pagination" style="color:floralwhite">
           <?php 
             if ($current_page > 1 && $total_page > 1){
                 echo '<a href="Shoesstore.php?page='.($current_page-1).'">Prev</a>  ';
              }

             for ($i = 1; $i <= $total_page; $i++){
                 if ($i == $current_page){
            echo '<span>'.$i.'</span> | ';
                }
                 else{
                    echo '<a href="Shoesstore.php?page='.$i.'">'.$i.'</a> | ';
                }
             }

            if ($current_page < $total_page && $total_page > 1){
                echo '<a href="Shoesstore.php?page='.($current_page+1).'">Next</a> | ';
            }
      
           ?>
        </div>

                </thead>
                    <?php

                    foreach($result as $row){
                       
                        // echo "<td> 
                        //  <a href=". "Shoesstore.php?edit=".$row['ID'] . " ".
                        //     "class="."btn btn-info>Edit</a>";
                    }
                    echo "</table>";
                    // if ($result->num_rows > 0) {
                    //   while ($row = $result->fetch_assoc()) {
                    //       echo "<tr><td>" . $row["id"] . "</td><td>" . $row["descriptions"] . "</td><td>" . $row["size"] . "</td><td>" . $row["price"] . "</td><tr>";
                    //   }
                      
                    // }
                    // else{
                    //   echo "0 result";
                    // }
                    
                    $conn-> close();            

                    ?>

            </div>
              </table>
        
    </div>
    
</main>
<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</body>
</html>